'''Planejamento de um algoritmo top-down

	PROBLEMA: No hotel Morte Lenta, estavam hospedados 5 hóspedes: Renato Faca,
Maria Cálice, Roberto Arma, Roberta Corda e Uesllei.
	À meia-noite, o hospede do quarto 101 foi encontrado morto, e os cinco
eram eles 5 . 
	Descubra quem é o assassino sabendo que: Se o nome do suspeito contém 3
vogais ou mais e nenhuma dessas vogais é "U" ou "o", ele pode ser o assassino.
	Se o suspeito for do sexo feminino, só será assassina se tiver utilizado uma
arma branca.
	Se o suspeito for do sexo masculino, ele deveria estar no saguão do hotel, às 00:30
para ser considerado o assassino.
	Sabendo dessas informações, elabore um algoritmo que fornecido o nome do suspeito,
indique se o mesmo pode ou não ser assassino.'''
	
print('')
print('MISTÉRIO DO HOTEL MORTE LENTA')
print('')

print('''	Suspeitos:

[1] Renato Faca
[2] Maria Cálice
[3] Roberto Arma
[4] Roberta Corda
[5] Uesllei	''')

print('')
arma = str(input('Digite o nome da ARMA: ')).lower().strip()
local = str(input('Digite o LOCAL do crime: ')).lower().strip()

while True:
	print('')
	nome = int(input('Escolha o assassino: '))
	if nome == 1:
		print('Você acha que Renato Faca matou com um(a) {} no(a) {}'.format(arma, local))
		if local == "saguao":
			print('Renato Faca é o assassino')
			break
		else:
			print('ERRADO!!!')

	elif nome == 2:
		print('Você acha que Maria Cálice matou com um(a) {} no(a) {} '.format(arma, local))			
		if arma == "faca":
			print('Maria Cálice é a assassina')
			break
		else:
			print('ERRADO!!!')

	elif nome == 3:
		print(f'Você acha que Roberto Arma matou com um(a) {arma} no(a) {local} ')
		if local == "saguao":
			print('Roberto Arma é o assassino')
			break
		else:
			print('ERRADO!!!')
	elif nome == 4:
		print(f'Você acha que Roberta Corda matou com um(a) {arma} no(a) {local} ')
		if arma == "faca":
			print('Roberta Corda é a assassina')
			break
		else:
			print('ERRADO!!!')


	elif nome == 5:
		print(f'Você acha que Uesllei matou com um(a) {arma} no(a) {local} ')
		if local == "saguao":
			print('Roberto Arma é o assassino')
			break
		else:
			print('ERRADO!!!')

		
	else:
		print('Número inválido')
